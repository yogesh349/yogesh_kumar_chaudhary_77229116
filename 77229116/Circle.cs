﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;


namespace _77229116
{
    /// <summary>
    /// Class Circle to draw and fill circle.
    /// </summary>
    class Circle : Shape
    {
        int radius;

        bool running = false;
        bool flag = false;

        private static object locker = new Object();

        public Circle() : base()
        {

        }
        /// <summary>
        /// Circle constructor to sets value
        /// </summary>
        /// <param name="coluor">Color of pen and brush</param>
        /// <param name="x">x-axis position of circle </param>
        /// <param name="y"> y-axis position of circle </param>
        /// <param name="radius">radius of circle</param>
        /// <param name="fill">bool fill if true fills shape with color</param>
        public Circle(Color coluor, int x, int y, int radius, bool fill, string blinkColor = "") : base(coluor, x, y, fill, blinkColor) // Created constructor to set values.
        {
            this.radius = radius;
        }


        public void blinkShape(Graphics g)
        {
            while (true)
            {
                while (running == true)
                {
                    if (blinkColor == "redgreen")
                    {
                        if (flag == false)
                        {
                            this.colour = System.Drawing.Color.Red;
                            flag = true;

                        }
                        else
                        {
                            this.colour = System.Drawing.Color.Green;
                            flag = false;

                        }

                        if(fill)
                        {
                            SolidBrush b = new SolidBrush(colour); 
                            g.FillEllipse(b, x, y, radius, radius);
                        } else
                        {
                            Pen p = new Pen(colour, 2); //pen object to create a new pen
                             g.DrawEllipse(p, x, y, radius, radius);
                        }

                        Thread.Sleep(500);

                    }
                    else if (blinkColor == "blueyellow")
                    {
                        if (flag == false)
                        {
                            this.colour = System.Drawing.Color.Blue;
                            flag = true;

                        }
                        else
                        {
                            this.colour = System.Drawing.Color.Yellow;
                            flag = false;

                        }

                        if (fill)
                        {
                            SolidBrush b = new SolidBrush(colour);
                            g.FillEllipse(b, x, y, radius, radius);
                        }
                        else
                        {
                            Pen p = new Pen(colour, 2); //pen object to create a new pen
                            g.DrawEllipse(p, x, y, radius, radius);
                        }

                        Thread.Sleep(500);

                    }
                    else if (blinkColor == "blackwhite")
                    {
                        if (flag == false)
                        {
                            this.colour = System.Drawing.Color.Black;
                            flag = true;

                        }
                        else
                        {
                            this.colour = System.Drawing.Color.White;
                            flag = false;

                        }

                        if (fill)
                        {
                            SolidBrush b = new SolidBrush(colour);
                            g.FillEllipse(b, x, y, radius, radius);
                        }
                        else
                        {
                            Pen p = new Pen(colour, 2); //pen object to create a new pen
                            g.DrawEllipse(p, x, y, radius, radius);
                        }

                        Thread.Sleep(500);

                    }
                }
            }
        }

        /// <summary>
        /// draw method to draw shapes of circle
        /// </summary>
        /// <param name="g">The object that is used to create graphical images.</param>
        public override void draw(Graphics g)
        {
            lock (locker)
            {
                if (blinkColor.Length > 0)
                {
                    running = !running;
                    Thread thread = new Thread(() => blinkShape(g));
                    thread.Start();
                }
                else
                {
                    Pen p = new Pen(colour, 2); //pen object to create a new pen
                    g.DrawEllipse(p, x, y, radius, radius); //draws eclipse
                }
            }
        }


        /// <summary>
        /// fillShape method to fill shapes of circle
        /// </summary>
        /// <param name="g"> The object that is used to create graphical images.</param>
        public override void fillShape(Graphics g)
        {
            lock (locker)
            {
                if (blinkColor.Length > 0)
                {
                    running = !running;
                    Thread thread = new Thread(() => blinkShape(g));
                    thread.Start();
                }
                else
                {
                    SolidBrush b = new SolidBrush(colour); // create a brush to fill shapes
                    g.FillEllipse(b, x, y, radius, radius); //fill eclipse
                }
            }
        }

    }
}
