﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Threading;


namespace _77229116
{
    /// <summary>
    /// Windows Form Class For Drawing Shpaes
    /// </summary>
    public partial class Form1 : Form
    {
        String[] richtextbox_cmd;
        String cmd_textbox;
        String richtextbox_cmd_store;
        Shape shape;
        int moveX, moveY;
        Color colour;
        bool fill;
        ArrayList shapes = new ArrayList();
        List<string[]> wlist = new List<string[]>();
        List<List<string>> addLines = new List<List<string>>();
        List<string> wconlist = new List<string>();
        Dictionary<string, int> My_dict1 = new Dictionary<string, int>();
        Dictionary<String, List<string[]>> methodDict = new Dictionary<string, List<string[]>>();
        bool check_if = false;
        bool isWhile = false;
        bool isBool;
        string nameM;
        bool flag = false, running = false;
        bool redgreen = false;
        bool isMethod = false;
        string blinkColor = "";
        List<Thread> threads = new List<Thread>();

        int wc = 0;
        public Form1()
        {
            InitializeComponent();
            ShapeFactory factory = new ShapeFactory();


        }

        private void richTextBox1_Input_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_Command_TextChanged(object sender, EventArgs e)
        {
            cmd_textbox = textBox1_Command.Text.ToLower();
        }

        /// <summary>
        /// execute_btn_CLick method to perform run, reset and clear action
        /// </summary>
        /// <param name="sender"> contains a reference to the control/object that raised the event.</param>
        /// <param name="e">A parameter that contains event data</param>
        private void execute_btn_Click(object sender, EventArgs e)
        {
           

            switch (cmd_textbox)
            {
                case "run":

                    try
                    {

                        ShapeFactory factory = new ShapeFactory();
                        //converting a string to lower case.
                        richtextbox_cmd_store = richTextBox1_Input.Text.ToLower();
                        //returns true if command richtextbox is empty and thows message.
                        if (richtextbox_cmd_store == String.Empty)
                        {
                            MessageBox.Show("Command or Richtextbox is empty. " +
                                "Please Type a valid Command");

                        }
                        char[] seperator = new char[] { '\n' };
                        //Gives an array of string if a richtextbox command contains new line
                        String[] richtextbox_cmd_array = richtextbox_cmd_store.Split(seperator, StringSplitOptions.RemoveEmptyEntries);

                        //Default colour for Shapes.
                        colour = Color.Black;
                        for (int i = 0; i < richtextbox_cmd_array.Length; i++)
                        {

                            String cmd_line = richtextbox_cmd_array[i]; //Takes an array value of richtextbox
                            char[] space_seperator = new char[] { ' ', ',' };
                            richtextbox_cmd = cmd_line.Split(space_seperator, StringSplitOptions.RemoveEmptyEntries); //Seperate a given array value if it contains commas or spaces.
                            
                           


                            if (richtextbox_cmd[0] == "method")
                            {
                         
                                isMethod = true;
                                nameM = richtextbox_cmd[1];
                                continue;
                            }

                            if (richtextbox_cmd[0] == "endmethod")
                            {
                                isMethod = false;
                                methodDict[nameM] = wlist;
                                nameM = "";
                                continue;
                            }

                            if (methodDict.ContainsKey(richtextbox_cmd[0]))
                            {
                                List<string[]> showL = methodDict[richtextbox_cmd[0]];
                                foreach(var show in showL)
                                {
                                    richtextbox_cmd = show;
                                    this.ExecuteDrawCommands();

                                }
                                continue;
                            }

                           

                            if (richtextbox_cmd[0] == "if")
                            {
                                string ifcon = richtextbox_cmd[1] + "" + richtextbox_cmd[2] + "" +
                                richtextbox_cmd[3];
                                wconlist.Add(ifcon);
                                string[] se = new string[] { "<=", ">=", "==", "<", ">","!=" };
                                string c = wconlist[wc];
                                String[] listt = c.Split(se, StringSplitOptions.RemoveEmptyEntries);

                                var rhs = 0;
                                var lhs = 0;

                                if (My_dict1.ContainsKey(listt[0]))
                                {
                                    lhs = My_dict1[listt[0]];
                                }
                                else
                                {

                                    //lhs = Convert.ToInt32(listt[0]);
                                }
                                if (My_dict1.ContainsKey(listt[1]))
                                {
                                    rhs = My_dict1[listt[1]];
                                }
                                else
                                {

                                    rhs = Convert.ToInt32(listt[1]);
                                }

                                if (c.Contains("=="))
                                {
                                    if (lhs == rhs)
                                    {
                                        check_if = false;
                                    }
                                    else
                                    {
                                        check_if = true;
                                    }
                                }else if (c.Contains(">"))
                                {
                                    if (lhs > rhs)
                                    {
                                        check_if = false;
                                    }
                                    else
                                    {
                                        check_if = true;
                                    }

                                }
                                else if (c.Contains("<"))
                                {
                                    if (lhs < rhs)
                                    {
                                        check_if = false;
                                    }
                                    else
                                    {
                                        check_if = true;
                                    }

                                }
                                else if (c.Contains("!="))
                                {
                                    if (lhs != rhs)
                                    {
                                        check_if = false;
                                    }
                                    else
                                    {
                                        check_if = true;
                                    }

                                }


                            }
                            else if (richtextbox_cmd[0] == "endif")
                            {
                                check_if = false;

                                continue;
                            }

                            /////////////////////////////////////////////
                            if (richtextbox_cmd[0] == "while")
                            {
                                isWhile = true;
                                String v_cond = richtextbox_cmd[1] + "" + richtextbox_cmd[2] + "" +
                                richtextbox_cmd[3];
                                wconlist.Add(v_cond);
                                continue;
                            }
                            else if (richtextbox_cmd[0] == "endloop")
                            {

                                isWhile = false;
                                do
                                {
                                    string[] se = new string[] {"<=", ">=", "==", "<", ">"};
                                    string c = wconlist[wc];
                                    String[] listt = c.Split(se, StringSplitOptions.RemoveEmptyEntries);


                                    var rhs = 0;
                                    var lhs = 0;

                                    if (My_dict1.ContainsKey(listt[0]))
                                    {
                                        lhs = My_dict1[listt[0]];
                                    }
                                    else
                                    {

                                        lhs = Convert.ToInt32(listt[0]);
                                    }
                                    if (My_dict1.ContainsKey(listt[1]))
                                    {
                                        rhs = My_dict1[listt[1]];
                                    }
                                    else
                                    {

                                        rhs = Convert.ToInt32(listt[1]);
                                    }
                                    
                                    if (c.Contains("<"))
                                    {
                                        if (lhs < rhs)
                                        {
                                            isBool = true;
                                        }
                                        else
                                        {
                                            isBool = false;

                                        }
                                        if (isBool == false)
                                        {
                                            break;
                                        }
                                        foreach (string[] w in wlist)
                                        {
                                            richtextbox_cmd = w;
                                            this.ExecuteDrawCommands();
                                        }


                                    }
                                    else if (c.Contains(">"))
                                    {
                                        if (lhs > rhs)
                                        {
                                            isBool = true;
                                        }
                                        else
                                        {
                                            isBool = false;

                                        }
                                        if (isBool == false)
                                        {
                                            break;
                                        }
                                        foreach (string[] w in wlist)
                                        {
                                            richtextbox_cmd = w;
                                            this.ExecuteDrawCommands();
                                        }


                                    }
                                } while (isBool == true);
                                
                                wc++;
                            }

                            this.ExecuteDrawCommands();
                        }


                        drawAllShapes();
                        shapes.Clear(); //clear AraayList
                        My_dict1.Clear();
                        wlist.Clear();
                        wconlist.Clear();
                        methodDict.Clear();
                    }
                    catch (Exception ef)
                    {
                        MessageBox.Show("Error :" + ef);
                    }
                    break;
                case "reset":
                    moveX = 0;
                    moveY = 0;
                    break;
                case "clear":
                    shapes.Clear(); //clear AraayList
                    moveX = 0;
                    moveY = 0;
                    fill = false;
                    textBox1_Command.Clear(); //clear command textbox.
                    richTextBox1_Input.Clear();
                    pictureBox1.Refresh(); //refresh picturebox.
                    break;

                default:
                    if (cmd_textbox == "")
                    {
                        MessageBox.Show("The command Line is empty");
                    }
                    else
                    {
                        MessageBox.Show("!!!! Invalid Input. Enter a valid input like \n" +
                            "run to execute commamnd,\n" +
                            "reset to reset command,\n"
                            + "clear to clear all.");
                    }
                    break;
            }
        }


        public void ExecuteDrawCommands()
        {
            ShapeFactory factory = new ShapeFactory();

            if (isWhile)
            {
                wlist.Add(richtextbox_cmd);
                return;
            }

            if (isMethod)
            {
                wlist.Add(richtextbox_cmd);
                return;
            }




            if (Regex.IsMatch(richtextbox_cmd[0], @"^[a-zA-Z]+$") && richtextbox_cmd[1] == "=")
            {
                int value = 0;
                if (richtextbox_cmd.Length == 5)
                {
                    string num1 = richtextbox_cmd[2].Trim();
                    string num2 = richtextbox_cmd[4].Trim();

                    int a, b;

                    if (My_dict1.ContainsKey(num1))
                    {
                        a = My_dict1[num1];                       
                    } 
                    else
                    {
                        a = int.Parse(num1);
                    }

                    if (My_dict1.ContainsKey(num2))
                    {
                        b = My_dict1[num2];
                    }
                    else
                    {
                        b = int.Parse(num2);
                    }

                    string op = richtextbox_cmd[3].Trim();

                    if (op == "+")
                    {
                        value = a + b;
                    }
                    else if (op == "*")
                    {
                        value = a * b;
                    }
                    else if (op == "-")
                    {
                        value = a * b;
                    }

                    My_dict1[richtextbox_cmd[0]] = value;
                }
                else
                {
                    value = Convert.ToInt32(richtextbox_cmd[2]);
                }

                My_dict1[richtextbox_cmd[0]] = value;

                return;
            }


            if (richtextbox_cmd[0] == "moveto")
            {

                if (Regex.IsMatch(richtextbox_cmd[1], @"^[a-zA-Z]+$") &&
                    Regex.IsMatch(richtextbox_cmd[2], @"^[a-zA-Z]+$"))
                {

                    moveX = My_dict1[richtextbox_cmd[1]];
                    moveY = My_dict1[richtextbox_cmd[2]];

                }
                else
                {

                    moveX = Convert.ToInt32(richtextbox_cmd[1]);//getting postion of x
                    moveY = Convert.ToInt32(richtextbox_cmd[2]); //getting postion of y


                }


            }
            try
            {

                if (richtextbox_cmd[0] == "circle")
                {
                    int radius;
                    if (Regex.IsMatch(richtextbox_cmd[1], @"^[a-zA-Z]+$"))
                    {
                        radius = My_dict1[richtextbox_cmd[1]];


                    }
                    else
                    {
                        radius = Convert.ToInt32(richtextbox_cmd[1]);

                    }

                    if (!(richtextbox_cmd.Length == 2) || radius < 0)  //returns true if its length is not eqaul to two.
                    {
                        MessageBox.Show("!! Invalid Command " + " Command Should Must Positive or  Be Like circle 30");

                    }
                    else
                    {
                  /*      while (running = !running)
                        {
                            shape = factory.getShape(richtextbox_cmd[0]);
                            shape = new Circle(colour, moveX, moveY, radius, fill); //passing arguments to set values in constructor.
                            shapes.Add(shape); //adding shapes to ArrayList.

                        }  */
                        if (check_if == false)
                        {
                            shape = factory.getShape(richtextbox_cmd[0]);
                            shape = new Circle(colour, moveX, moveY, radius, fill, blinkColor); //passing arguments to set values in constructor.
                            shapes.Add(shape); //adding shapes to ArrayList.w

                        }


                    }

                }

            }
            catch (Exception ef)
            {
                MessageBox.Show(" Cannot convert String " + richtextbox_cmd[1] + " into integer.\n" +
                    "Please enter a integer number");
                MessageBox.Show("Error: " + ef);

            }
            try
            {
                if (richtextbox_cmd[0] == "rectangle")
                {
                    int width;
                    int height;

                    if (Regex.IsMatch(richtextbox_cmd[1], @"^[a-zA-Z]+$") &&
                        Regex.IsMatch(richtextbox_cmd[2], @"^[a-zA-Z]+$"))
                    {
                        width = My_dict1[richtextbox_cmd[1]];
                        height = My_dict1[richtextbox_cmd[2]];


                    }
                    else
                    {
                        width = Convert.ToInt32(richtextbox_cmd[1]);
                        height = Convert.ToInt32(richtextbox_cmd[2]);

                    }
                    //check for length of given command array will give true if its length is not equal to 3.
                    if (!(richtextbox_cmd.Length == 3) || width < 0 || height < 0)
                    {
                        MessageBox.Show("!!!! Invalid Input . Input Should be Positive and  like rectangle 30 40");
                    }
                    else
                    {
                        if (check_if == false)
                        {
                            shape = factory.getShape(richtextbox_cmd[0]);
                            shape = new Rectangle(colour, moveX, moveY, width, height
                            , fill, blinkColor); //passing arguments to set values in constructor.
                            shapes.Add(shape); //adding shape to ArrayList
                        }


                    }


                }

            }
            catch (Exception ef)
            {
                MessageBox.Show("Error: " + ef);
            }

            try
            {
                if (richtextbox_cmd[0] == "triangle")
                {
                    if (!(richtextbox_cmd.Length == 5)) //returns true if length not equal to 5
                    {
                        MessageBox.Show("Invalid Input " + " Input Must Be Like triangle 20 20 10 40");

                    }
                    else
                    {
                        int x1;
                        int y1;
                        int x2;
                        int y2;
                        if (Regex.IsMatch(richtextbox_cmd[1], @"^[a-zA-Z]+$") &&
                            Regex.IsMatch(richtextbox_cmd[2], @"^[a-zA-Z]+$") &&
                            Regex.IsMatch(richtextbox_cmd[3], @"^[a-zA-Z]+$") &&
                            Regex.IsMatch(richtextbox_cmd[4], @"^[a-zA-Z]+$"))
                        {
                            x1 = My_dict1[richtextbox_cmd[1]];
                            y1 = My_dict1[richtextbox_cmd[2]];
                            x2 = My_dict1[richtextbox_cmd[3]];
                            y2 = My_dict1[richtextbox_cmd[4]];
                        }
                        else
                        {
                            x1 = Convert.ToInt32(richtextbox_cmd[1]);
                            y1 = Convert.ToInt32(richtextbox_cmd[2]);
                            x2 = Convert.ToInt32(richtextbox_cmd[3]);
                            y2 = Convert.ToInt32(richtextbox_cmd[4]);
                        }

                        if (check_if == false)
                        {
                            shape = factory.getShape(richtextbox_cmd[0]);
                            Point[] point = { new Point(moveX,moveY),
                                            new Point(x1, y1),
                                            new Point(x2, y2) };  //passing value to array point
                            shape = new Triangle(point, colour, moveX, moveY, fill, blinkColor); //passing arguments to set values in constructor.
                            shapes.Add(shape); //adding shape to ArrayList

                        }

                    }
                }


            }
            catch (Exception ef)
            {
                MessageBox.Show("Error: " + ef);
            }



            if (richtextbox_cmd[0] == "pen")
            {
               
                if (!(richtextbox_cmd.Length == 2)) //returns true if length is not equal to 2
                {
                    MessageBox.Show("Invalid Input " + "Command Must Be Like pen red");
                }
                else
                {

                    if (check_if == false)
                    {
                        //Selecting  Colors
                        if (richtextbox_cmd[1] == "red")
                        {
                            colour = Color.Red;
                        }
                        else if (richtextbox_cmd[1] == "blue")
                        {
                            colour = Color.Blue;
                        }
                        else if (richtextbox_cmd[1] == "yellow")
                        {

                            colour = Color.Yellow;
                        }
                        else if (richtextbox_cmd[1] == "orange")
                        {

                            colour = Color.Orange;
                        }
                        else if (richtextbox_cmd[1] == "redgreen")
                        {
                            blinkColor = "redgreen";
                        }
                        else if (richtextbox_cmd[1] == "blueyellow")
                        {
                            blinkColor = "blueyellow";
                        }
                        else if (richtextbox_cmd[1] == "blackwhite")
                        {
                            blinkColor = "blackwhite";
                        }
                        else
                        {
                            MessageBox.Show("Invalid Color");
                            
                        }

                    }


                }
            }
            try
            {
                if (richtextbox_cmd[0] == "fill")
                {

                    if (!(richtextbox_cmd.Length == 2))
                    {
                        MessageBox.Show("!!!!Invalid Command Command must be like fill on/off");

                    }
                    else
                    {
                        if (check_if == false)
                        {
                            if (richtextbox_cmd[1] == "on")
                            {
                                fill = true; //fills equals to true if fill on.

                            }
                            else if (richtextbox_cmd[1] == "off")
                            {
                                fill = false; //fills equals to true if fill off.
                            }
                            else
                            {
                                MessageBox.Show("!!! Invalid Input. Input must be like fill on/off");
                            }
                        }


                    }


                }

            }
            catch (Exception ef)
            {

                MessageBox.Show("Error " + ef);
            }


            try
            {

                if (richtextbox_cmd[0] == "drawto")
                {

                    if (!(richtextbox_cmd.Length == 3)) //return true if length not equals to three.
                    {
                        MessageBox.Show("!!! Invalid Input. Input Must Be like drawto 23 45");

                    }
                    else
                    {
                        int x1, y1;
                        if (Regex.IsMatch(richtextbox_cmd[1], @"^[a-zA-Z]+$") &&
                        Regex.IsMatch(richtextbox_cmd[2], @"^[a-zA-Z]+$"))
                        {
                            x1 = My_dict1[richtextbox_cmd[1]];
                            y1 = My_dict1[richtextbox_cmd[2]];


                        }
                        else
                        {
                            x1 = Convert.ToInt32(richtextbox_cmd[1]);
                            y1 = Convert.ToInt32(richtextbox_cmd[2]);

                        }
                        if (check_if == false)
                        {
                            shape = new Line();
                            shape = new Line(colour, moveX, moveY, x1, y1); //passing arguments to set values in constructor.
                            shapes.Add(shape); //adding shape to ArrayList
                        }

                    }
                }

            }
            catch (Exception ef)
            {
                MessageBox.Show("Error: " + ef);
            }
            //Showing an error when richtextbox dont contains 
            if (!(richtextbox_cmd[0] == "circle") && (!(richtextbox_cmd[0] == "rectangle")) &&
                (!(richtextbox_cmd[0] == "triangle")) && (!(richtextbox_cmd[0] == "pen"))
                 && (!(richtextbox_cmd[0] == "fill")) && (!(richtextbox_cmd[0] == "moveto")) &&
                 (!(richtextbox_cmd[0] == "drawto")) && (!(richtextbox_cmd[1] == "="))
                 && (!(richtextbox_cmd[0] == "if")))
            {
                MessageBox.Show("!! Invalid Command Please enter a valid command like,\n" +
                    "\n" +
                    "rectangle 10 20,\n" +
                    "\n" +
                    "circle 120, \n" +
                    "\n" +
                    "triangle 20 20 30 40,\n" +
                    "\n" +
                    "pen red,\n" +
                    "\n" +
                    "fill on/off,\n" +
                    "\n" +
                    "moveto 10 30,\n" +
                    "\n" +
                    "drawto 40 40.\n", "Richtextbox or CommandLine Error");

            }
        }


        /// <summary>
        /// pictureBox1_Paint method to paint shapes in Picturebox
        /// </summary>
        /// <param name="sender"> contains a reference to the control/object that raised the event.</param>
        /// <param name="e">A parameter that contains event data</param>
        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void drawAllShapes()
        {
            Graphics g = pictureBox1.CreateGraphics();
            //loops through a Arraylist of shapes
            for (int i = 0; i < shapes.Count; i++)
            {
                shape = (Shape)shapes[i];

                if (shape.xfill() == true)
                {
                    shape.fillShape(g);
                }
                else
                {
                    shape.draw(g);
                }
            }
        }



        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("This App helps us to  draw some shapes like " +
                 "\n" +
                 "\n" +
                "rectangle,triangle and circle. " +
                 "\n" +
                 "\n" +
                "This app is useful to draw easy shapes.  " +
                 "\n" +
                 "\n" +
                "With this assignment i got easily adapted in C#" +
                 "\n" +
                 "\n" +
                "Got a sharp knowledge on OOPs, C# GUI.", "About");
        }

        private void exitToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Exit(); //Exit syntax, when clicked exit it exit from the apps.
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            if (sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                File.WriteAllText(sfd.FileName, richTextBox1_Input.Text); //Save Files in computer.
            }
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var dlg = new OpenFileDialog();
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                richTextBox1_Input.Text = File.ReadAllText(dlg.FileName); //load files in richtextbox
            }
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("To use this app follo those commands given below\n" +
                 "\n" +
                "************************************\n" +
                 "\n" +
                "For Circle, Type circle <radius>  \n" +
                 "\n" +
                   "************************************\n" +
                    "\n" +
                "For Rectangle , Type  rectangle <width>, <height> \n" +
                "\n" +
                  "************************************\n" +
                   "\n" +
                "For pen or brush color type pen <colour>   \n" +
                 "\n" +
                   "************************************\n" +
                "To fill or unfill shapes type fill <on/off>\n" +
                 "\n" +
                   "************************************\n" +
                    "\n" +
                "To move write moveTo x y \n" +
                 "\n" +
                   "************************************\n" +
                "Write clear in textbox to clear drawing area/command box/commandline/ \n" +
                 "\n" +
                   "************************************\n" +
                    "\n" +
                "Write reset to move pen to initial position at the top of the screen", "Help");
        }


    }
}
