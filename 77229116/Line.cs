﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _77229116
{
    /// <summary>
    ///Line Class  to draw straight line
    /// </summary>
    class Line : Shape
    {
        int x2, y2;
        public Line()
        {

        }
        

        /// <summary>
        ///Constructor to set Values
        /// </summary>
        /// <param name="colour">Color of pen and brush</param>
        /// <param name="x1">upper x position of line</param>
        /// <param name="y1">upper y position of line</param>
        /// <param name="x2">lower x position of line</param>
        /// <param name="y2">lower y position of line</param>
        public Line(Color colour,int x1, int y1, int x2, int y2)
        {
            this.colour = colour;
            this.x = x1;
            this.y = y1;
            this.x2 = x2;
            this.y2 = y2;

        }
        /// <summary>
        /// method draw to draw line
        /// </summary>
        /// <param name="g">The object that is used to create graphical images.</param>
        public override void draw(Graphics g)
        {
            Pen p = new Pen(colour, 2); //pen object to create a new pen
            g.DrawLine(p, x, y, x2, y2); //draw line 


        }


        /// <summary>
        /// method fillShape to fill shapes of Line
        /// </summary>
        /// <param name="g">The object that is used to create graphical images</param>
        public override void fillShape(Graphics g)
        {
            throw new NotImplementedException();
        }
    }
}
