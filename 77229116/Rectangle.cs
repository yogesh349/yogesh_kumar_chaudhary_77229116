﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Threading;

namespace _77229116
{
    /// <summary>
    /// Class Rectangle to draw and fill rectangle 
    /// </summary>
    class Rectangle : Shape
    {
        int width, height;
        bool running = false;
        bool flag = false;
        private static object locker = new Object();
        public Rectangle() : base()
        {
            width = 100;
            height = 100;
        }
        /// <summary>
        /// Rectangle constructor to set values of properties
        /// </summary>
        /// <param name="colour"> Color of pen and brush</param>
        /// <param name="x">x-axis position of circle</param>
        /// <param name="y">  y-axis position of circle </param>
        /// <param name="width">width of column </param>
        /// <param name="height">height of column</param>
        /// <param name="fill">bool fill if true fills shape with color</param>
        public Rectangle(Color colour, int x, int y, int width, int height, bool fill, string blinkColor = "") : base(colour, x, y, fill, blinkColor) // Created constructor to set values.
        {

            this.width = width; //the only thing that is different from shape
            this.height = height;
        }


        public void blinkShape(Graphics g)
        {
            while (true)
            {
                while (running == true)
                {
                    if (blinkColor == "redgreen")
                    {
                        if (flag == false)
                        {
                            this.colour = System.Drawing.Color.Red;
                            flag = true;

                        }
                        else
                        {
                            this.colour = System.Drawing.Color.Green;
                            flag = false;

                        }

                        if (fill)
                        {
                            SolidBrush b = new SolidBrush(colour);
                            g.FillRectangle(b, x, y, width, height);
                        }
                        else
                        {
                            Pen p = new Pen(colour, 2); //pen object to create a new pen
                            g.DrawRectangle(p, x, y, width, height);
                        }

                        Thread.Sleep(500);

                    }else if (blinkColor == "blueyellow")
                    {
                        if (flag == false)
                        {
                            this.colour = System.Drawing.Color.Blue;
                            flag = true;

                        }
                        else
                        {
                            this.colour = System.Drawing.Color.Yellow;
                            flag = false;

                        }

                        if (fill)
                        {
                            SolidBrush b = new SolidBrush(colour);
                            g.FillRectangle(b, x, y, width, height);
                        }
                        else
                        {
                            Pen p = new Pen(colour, 2); //pen object to create a new pen
                            g.DrawRectangle(p, x, y, width, height);
                        }

                        Thread.Sleep(500);

                    }
                    else if (blinkColor == "blackwhite")
                    {
                        if (flag == false)
                        {
                            this.colour = System.Drawing.Color.Black;
                            flag = true;

                        }
                        else
                        {
                            this.colour = System.Drawing.Color.White;
                            flag = false;

                        }

                        if (fill)
                        {
                            SolidBrush b = new SolidBrush(colour);
                            g.FillRectangle(b, x, y, width, height);
                        }
                        else
                        {
                            Pen p = new Pen(colour, 2); //pen object to create a new pen
                            g.DrawRectangle(p, x, y, width, height);
                        }

                        Thread.Sleep(500);

                    }
                }
            }
        }

        /// <summary>
        /// draw method to draw rectangle
        /// </summary>
        /// <param name="g">The object that is used to create graphical images</param>
        public override void draw(Graphics g)
        {
           // Pen p = new Pen(colour, 2); //create a pen
            //g.DrawRectangle(p, x, y, width, height); //draw rectangle 

            lock (locker)
            {
                if (blinkColor.Length > 0)
                {
                    running = !running;
                    Thread thread = new Thread(() => blinkShape(g));
                    thread.Start();
                }
                else
                {
                    Pen p = new Pen(colour, 2); //pen object to create a new pen
                    g.DrawRectangle(p, x, y, width, height); //draws eclipse
                }
            }

        }

        /// <summary>
        /// fillShape method to fill shape of rectangle
        /// </summary>
        /// <param name="g">The object that is used to create graphical images</param>
        public override void fillShape(Graphics g)
        {

            lock (locker)
            {
                if (blinkColor.Length > 0)
                {
                    running = !running;
                    Thread thread = new Thread(() => blinkShape(g));
                    thread.Start();
                }
                else
                {
                    SolidBrush b = new SolidBrush(colour); // create a brush to fill shapes
                    g.FillRectangle(b, x, y, width, height); //fill eclipse
                }
            }
           // SolidBrush b = new SolidBrush(colour); // create a brush to fill shapes
            //g.FillRectangle(b, x, y, width, height); //fill eclipse
        }
    }

}
