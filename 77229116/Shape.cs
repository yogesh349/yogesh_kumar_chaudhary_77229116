﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace _77229116
{
    /// <summary>
    /// Abstract shape class where implemented and not implemented methods.
    /// </summary>
   abstract public class Shape:Shapes
    {

        protected Color colour; //shape's colour
        protected int x, y;
        protected bool fill;
        protected string blinkColor = "";

        public Shape()
        {
            colour = Color.Red;
            x = y = 100;
        }

        /// <summary>
        /// Shape constructor to set values
        /// </summary>
        /// <param name="colour"> color to set color of shapes</param>
        /// <param name="x">x-cordinate of Position</param>
        /// <param name="y">y-cordinate of position</param>
        /// <param name="fill">fill to fill shapes and unfill shapes</param>
        public Shape(Color colour, int x, int y, bool fill, string blinkColor="")
        {

            this.colour = colour; //shape's colour
            this.x = x; //its x pos
            this.y = y; //its y pos
            this.fill = fill;
            this.blinkColor = blinkColor;
            //can't provide anything else as "shape" is too general
        }

        public string BlinkColor
        {
            get { return this.blinkColor; }
            set { this.blinkColor = value; }
        }


        /// <summary>
        /// draw method to draw shapes
        /// </summary>
        /// <param name="g">The object that is used to create graphical images</param>
        public abstract void draw(Graphics g);

        /// <summary>
        /// fillShape method to fill shapes
        /// </summary>
        /// <param name="g">The object that is used to create graphical images</param>
        public abstract void fillShape(Graphics g);


        /// <summary>
        /// xfill to return fill bool values
        /// </summary>
        /// <returns></returns>
        public bool xfill()
        {
            return fill;
        }

        public override string ToString()
        {
            return base.ToString() + "    " + this.x + "," + this.y + " : ";
        }

    }
}
