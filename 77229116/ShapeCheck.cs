﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _77229116
{
    /// <summary>
    /// ShapaeCheck Class to check whether shape exits or not.
    /// </summary>
    public class ShapeCheck
    {
        /// <summary>
        /// isCIrcle methodd to check if a given string is circle or not.
        /// </summary>
        /// <param name="shape"> shape param to take shapes</param>
        /// <returns>if its rectangle will return true else it will return false</returns>
        public bool isCircle(String shape)
        {
            if (shape == "circle")
            {
                return true;
            }
            else
            {
                return false;
            }

        }


        /// <summary>
        /// isRectanlge methodd to check if a given string is rectangle or not.
        /// </summary>
        /// <param name="shape"> shape param to take shapes</param>
        /// <returns>if its rectangle will return true else it will return false</returns>
        public bool isRectangle(String shape)
        {
            if (shape == "rectangle")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// isTriangle methodd to check if a given string is triangle or not.
        /// </summary>
        /// <param name="shape"> shape param to take shapes</param>
        /// <returns>if its triangle will return true else it will return false</returns>
        public bool isTriangle(String shape)
        {
            if (shape == "triangle")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
