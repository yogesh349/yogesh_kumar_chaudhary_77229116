﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _77229116
{
    /// <summary>
    /// ShapeFactory class to give object of shapes
    /// </summary>
    class ShapeFactory
    {
         /// <summary>
         /// getShape method returns an object of shapes.
         /// </summary>
         /// <param name="shapeType">define types of shape</param>
         /// <returns></returns>
        public Shape getShape(String shapeType)
        {
            shapeType = shapeType.ToLower().Trim(); //you could argue that you want a specific word string to create an object but I'm allowing any case combination


            if (shapeType.Equals("circle"))
            {
                return new Circle();

            }
            else if(shapeType.Equals("rectangle"))
            {
                return new Rectangle();

            }
            else if(shapeType.Equals("triangle"))
            {
                return new Triangle();
            }
            else if(shapeType.Equals("line"))
            {
                return new Line();
            }
            else
            {
                //if we get here then what has been passed in is unknown so throw an appropriate exception
                System.ArgumentException argEx = new System.ArgumentException("Factory error: " + shapeType + " does not exist");
                throw argEx;
            }


        }
    }

}

