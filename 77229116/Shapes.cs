﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace _77229116
{
    /// <summary>
    /// Shapes interface haveing unimplemented methods
    /// </summary>
    interface Shapes
    {
        /// <summary>
        /// daw method to draw shapes
        /// </summary>
        /// <param name="g">The object that is used to create graphical images</param>
        void draw(Graphics g);



        /// <summary>
        /// fillShape method to fill shapes
        /// </summary>
        /// <param name="g">The object that is used to create graphical images</param>
        void fillShape(Graphics g);

    }
}
