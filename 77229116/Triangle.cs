﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _77229116
{
    /// <summary>
    /// Triangle class to draw and fill triangle
    /// </summary>
    class Triangle : Shape
    {
        Point[] point;
        bool running = false;
        bool flag = false;
        private static object locker = new Object();
        public Triangle()
        {

        }


        /// <summary>
        /// Triangle constructor to set properties
        /// </summary>
        /// <param name="point">array of Points where X and a Y coordinate grouped together</param>
        /// <param name="colour">Color of pen and brush</param>
        /// <param name="x">x-axis position of circle</param>
        /// <param name="y">y-axis position of circle</param>
        /// <param name="fill"> bool fill if true fills shape with color</param>
        public Triangle(Point[] point,Color colour,int x, int y, bool fill, string blinkColor = "") :base(colour,x,y,fill, blinkColor) // Created constructor to set values.
        {
            this.point = point;
        }

        public void blinkShape(Graphics g)
        {
            while (true)
            {
                while (running == true)
                {
                    if (blinkColor == "redgreen")
                    {
                        if (flag == false)
                        {
                            this.colour = System.Drawing.Color.Red;
                            flag = true;

                        }
                        else
                        {
                            this.colour = System.Drawing.Color.Green;
                            flag = false;

                        }

                        if (fill)
                        {
                            SolidBrush b = new SolidBrush(colour);
                            g.FillPolygon(b, point);
                        }
                        else
                        {
                            Pen p = new Pen(colour, 2); //pen object to create a new pen
                            g.DrawPolygon(p, point);
                        }

                        Thread.Sleep(500);

                    }
                    else if (blinkColor == "blueyellow")
                    {
                        if (flag == false)
                        {
                            this.colour = System.Drawing.Color.Blue;
                            flag = true;

                        }
                        else
                        {
                            this.colour = System.Drawing.Color.Yellow;
                            flag = false;

                        }

                        if (fill)
                        {
                            SolidBrush b = new SolidBrush(colour);
                            g.FillPolygon(b, point);
                        }
                        else
                        {
                            Pen p = new Pen(colour, 2); //pen object to create a new pen
                            g.DrawPolygon(p, point);
                        }

                        Thread.Sleep(500);

                    }
                    else if (blinkColor == "blackwhite")
                    {
                        if (flag == false)
                        {
                            this.colour = System.Drawing.Color.Black;
                            flag = true;

                        }
                        else
                        {
                            this.colour = System.Drawing.Color.White;
                            flag = false;

                        }

                        if (fill)
                        {
                            SolidBrush b = new SolidBrush(colour);
                            g.FillPolygon(b, point);
                        }
                        else
                        {
                            Pen p = new Pen(colour, 2); //pen object to create a new pen
                            g.DrawPolygon(p,point);
                        }

                        Thread.Sleep(500);

                    }
                }
            }
        }

        /// <summary>
        /// draw method to draw triangle
        /// </summary>
        /// <param name="g">The object that is used to create graphical images</param>
        public override void draw(Graphics g)
        {
            lock (locker)
            {
                if (blinkColor.Length > 0)
                {
                    running = !running;
                    Thread thread = new Thread(() => blinkShape(g));
                    thread.Start();
                }
                else
                {
                    Pen p = new Pen(colour, 2); //pen object to create a new pen
                    g.DrawPolygon(p, point); //draws eclipse
                }
            }
    
        }

        /// <summary>
        /// fillShape method to fill shape of triangle
        /// </summary>
        /// <param name="g">The object that is used to create graphical images</param>
        public override void fillShape(Graphics g)
        {
    
            lock (locker)
            {
                if (blinkColor.Length > 0)
                {
                    running = !running;
                    Thread thread = new Thread(() => blinkShape(g));
                    thread.Start();
                }
                else
                {
                    SolidBrush b = new SolidBrush(colour); // create a brush to fill shapes
                    g.FillPolygon(b, point); //fill eclipse
                }
            }


        } 
    }
}
