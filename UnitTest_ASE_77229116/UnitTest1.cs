﻿using _77229116;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace UnitTest_ASE_77229116
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestTriangleShape()
        {
            var shapeCheck = new ShapeCheck();
            bool result = shapeCheck.isTriangle("triangle");
            Assert.AreEqual(true, result);
        }



        [TestMethod]
        public void TestRectangleShape()
        {
            var shapeCheck = new ShapeCheck();
            bool result = shapeCheck.isRectangle("rectangle");
            Assert.AreEqual(true, result);
        }



        [TestMethod]
        public void TestCircleShape()
        {
            var shapeCheck = new ShapeCheck();
            bool result = shapeCheck.isCircle("circle");
            Assert.AreEqual(true, result);
        }

       


    }
}
